<?php defined('C5_EXECUTE') or die("Access Denied."); ?> 
<h2>Go</h2>
<div class="row">
    <div class="col-md-12">
        <h3>Options</h3>
        <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                    <title>Layout</title>
                    <select name="yhi_01">
                        <option value="6x6"     <?php //if($yhi_01 == '6x6')  echo "selected"; ?>>    _6 6_ </option>
                        <option value="4x8"     <?php //if($yhi_01 == '4x8')  echo "selected"; ?>>     4 _8   </option>
                        <option value="8x4"     <?php //if($yhi_01 == '8x4') echo "selected"; ?>>      8_ 4   </option>
                        <option value="2x4x4x2" <?php //if($yhi_01 == '2x4x4x2')  echo "selected"; ?>> _4 4_</option>
                    </select>
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group">
                    <label>Border</label>
                    <select name="yhi_border_01">
                        <option value="no-border"     <?php //if($yhi_border_01 == 'no-border')    echo "selected"; ?>> No Border </option>
                        <option value="out-border"    <?php //if($yhi_border_01 == 'out-border')   echo "selected"; ?>> Out Border </option>
                        <option value="block-border"  <?php //if($yhi_border_01 == 'block-border') echo "selected"; ?>> Block Border </option>
                    </select>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-6">
        <div class="form-group">
            <?php // h($left_uid_01);?>
            <title>Business Name</title>
            <h6>Business Name</h6>
            <input type="text" name="business_name"  value="<?php echo h($business_name);?>" />
        </div>
        <div class="form-group">
            <title>Business Email</title>
            <h6>Business Email</h6>
            <input type="text" name="business_email" value="<?php echo h($business_email); ?>" />
        </div>
        <div class="form-group">
            <title>Product Pitch</title>
            <h6>Product Pitch</h6>
            <input type="text" name="business_pitch" value="<?php echo h($business_pitch); ?>" />
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group">
            <?php // h($right_uid_01);?>
            <title>Business Image</title>
            <h6>Business Image</h6>
            <input type="text" name="business_image" value="<?php echo h($business_email); ?>" />
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-12">
        <h3>Options</h3>
        <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                    <title>Layout</title>
                    <select name="yhi_02">
                        <option value="6x6"     <?php //if($yhi_02 == '6x6')  echo "selected"; ?>>    _6 6_ </option>
                        <option value="4x8"     <?php //if($yhi_02 == '4x8')  echo "selected"; ?>>     4 _8   </option>
                        <option value="8x4"     <?php //if($yhi_02 == '8x4') echo "selected"; ?>>      8_ 4   </option>
                        <option value="2x4x4x2" <?php //if($yhi_02 == '2x4x4x2')  echo "selected"; ?>> _4 4_</option>
                    </select>
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group">
                    <label>Border</label>
                    <select name="yhi_border_02">
                        <option value="no-border"     <?php //if($yhi_border_02 == 'no-border')    echo "selected"; ?>> No Border </option>
                        <option value="out-border"    <?php //if($yhi_border_02 == 'out-border')   echo "selected"; ?>> Out Border </option>
                        <option value="block-border"  <?php //if($yhi_border_02 == 'block-border') echo "selected"; ?>> Block Border </option>
                    </select>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-6">
        <div class="form-group">
            <h3>simpleCart_shelfItem</h3>
            <title>Item Name</title>
            <h6>Item Name</h6>
            <input type="text" name="item_name" value="<?php echo h($item_name); ?>"/>
            <title>Item Price</title>
            <h6>Item Price</h6>
            <input type="text" name="item_price" value="<?php echo h($item_price); ?>"/>
            <!-- <title>Item Size</title> -->
            <!-- <input type="text" name="item_size"/> -->
            <!-- <title>Item Quantity</title> -->
            <!-- <input type="text" name="item_Quantity"/> -->

        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group">
            <textarea style="display:none;" 
            id="right_uid_02" name="right_uid_02"></textarea>
            <div id="right_uid_02_html" class="uid">
            <?php //echo h($right_uid_02);?></div>
        </div>
    </div>
</div>
<?php /* 

<style type="text/css">
    .uid {
        width: 100%;
        border: 1px solid #eee;
        height: 150px;
    }
</style>

<script type="text/javascript">
    $(function() {
        // var edit = function(para){

        //     var editor = ace.edit(para+ '_html');
        //     console.log('do', editor)
        //     editor.setTheme("ace/theme/eclipse");
        //     editor.getSession().setMode("ace/mode/html");
        //     refreshTextarea(editor.getValue());
        //     editor.getSession().on('change', function() {
        //         refreshTextarea(editor.getValue(), para);
        //     });
        // }
        
        // function refreshTextarea(contents, para) {
        //     // console.log('co', contents)
        //     $('#' +para).val(contents);
        // }

        // edit('left_uid_01')
        // edit('left_uid_02')
        // edit('left_uid_04')
        // edit('right_uid_01')
        // edit('right_uid_02')
        // edit('right_uid_04')
    });

</script>

*/?>