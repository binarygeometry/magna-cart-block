<?php
namespace Application\Block\MagnaCart;

use Loader;
use \Concrete\Core\Block\BlockController;

class Controller extends BlockController
{
    protected $btTable = 'btMagnaCart';
    protected $btInterfaceWidth = "600";
    protected $btWrapperClass = 'ccm-ui';
    protected $btInterfaceHeight = "500";
    protected $btCacheBlockRecord = true;
    protected $btCacheBlockOutput = true;
    protected $btCacheBlockOutputOnPost = true;
    protected $btCacheBlockOutputForRegisteredUsers = true;
    protected $btIgnorePageThemeGridFrameworkContainer = true;

    public $content = "";

    public function getBlockTypeDescription()
    {
        return t("For selling business.");
    }

    public function getBlockTypeName()
    {
        return t("MagnaCart");
    }

    public function view()
    {
        $this->set('content', $this->content);
    }

    public function add()
    {
        $this->edit();
    }

    public function edit()
    {
        $this->requireAsset('ace');
    }

    public function getSearchableContent()
    {
        return $this->content;
    }

    // public function processSale($data)
    // {
    //     $args['item_name'] = isset($data['item_name']) ? $data['item_name'] : '';
    //     $args['item_size'] = isset($data['item_size']) ? $data['item_size'] : '';
    //     $args['item_Quantity'] = isset($data['item_Quantity']) ? $data['item_Quantity'] : '';
    //     $args['item_price'] = isset($data['item_price']) ? $data['item_price'] : '';
    //     $args['seller'] = isset($data['seller']) ? $data['seller'] : '';
    //     $args['seller_email'] = isset($data['seller_email']) ? $data['seller_email'] : '';
    //     $args['buyer_email'] = isset($data['buyer_email']) ? $data['buyer_email'] : '';
    //     $args['invoice'] = isset($data['invoice']) ? $data['invoice'] : '';
    //     $args['misc'] = isset($data['misc']) ? $data['misc'] : '';
    // }

    public function on_start()
    {
        $al = \Concrete\Core\Asset\AssetList::getInstance();
        $al->register(
            'javascript', 'simple_cart', 'blocks/magna_cart/js/simpleCart.js',
            array('version' => '1.0.0', 'minify' => false, 'combine' => true)
        );
        $al->register(
            'css', 'magna_cart_css', 'blocks/magna_cart/magna-cart.css',
            array('version' => '1.0.0', 'minify' => false, 'combine' => true)
        );
        $al->registerGroup('magna_cart', array(
            array('css', 'magna_cart_css'),
            array('javascript', 'simple_cart')
        ));
    }


    public function registerViewAssets()
    {
        $this->requireAsset('magna_cart');
    }

    public function save($data)
    {
            $args['business_name'] = isset($data['business_name']) ? $data['business_name'] : '';
            $args['business_email'] = isset($data['business_email']) ? $data['business_email'] : '';
            $args['business_pitch'] = isset($data['business_pitch']) ? $data['business_pitch'] : '';
            $args['business_image'] = isset($data['business_image']) ? $data['business_image'] : '';
            $args['yhi_01'] = isset($data['yhi_01']) ? $data['yhi_01'] : '';
            $args['yhi_02'] = isset($data['yhi_02']) ? $data['yhi_02'] : '';
            $args['yhi_border_01'] = isset($data['yhi_border_01']) ? $data['yhi_border_01'] : '';
            $args['yhi_border_02'] = isset($data['yhi_border_02']) ? $data['yhi_border_02'] : '';
            $args['item_name'] = isset($data['']) ? $data['item_name'] : 'item_name';
            $args['item_price'] = isset($data['item_price']) ? $data['item_price'] : '';
            $args['item_size'] = isset($data['item_size']) ? $data['item_size'] : '';
            $args['item_Quantity'] = isset($data['item_Quantity']) ? $data['item_Quantity'] : '';
            $args['right_uid_02'] = isset($data['right_uid_02']) ? $data['right_uid_02'] : '';
            $args['misc']  = isset($data['misc'])  ? $data['misc']  : 'false';
        parent::save($args);
    }

    public static function xml_highlight($s)
    {
        $s = htmlspecialchars($s);
        $s = preg_replace(
            "#&lt;([/]*?)(.*)([\s]*?)&gt;#sU",
            "<font color=\"#0000FF\">&lt;\\1\\2\\3&gt;</font>",
            $s
        );
        $s = preg_replace(
            "#&lt;([\?])(.*)([\?])&gt;#sU",
            "<font color=\"#800000\">&lt;\\1\\2\\3&gt;</font>",
            $s
        );
        $s = preg_replace(
            "#&lt;([^\s\?/=])(.*)([\[\s/]|&gt;)#iU",
            "&lt;<font color=\"#808000\">\\1\\2</font>\\3",
            $s
        );
        $s = preg_replace(
            "#&lt;([/])([^\s]*?)([\s\]]*?)&gt;#iU",
            "&lt;\\1<font color=\"#808000\">\\2</font>\\3&gt;",
            $s
        );
        $s = preg_replace(
            "#([^\s]*?)\=(&quot;|')(.*)(&quot;|')#isU",
            "<font color=\"#800080\">\\1</font>=<font color=\"#FF00FF\">\\2\\3\\4</font>",
            $s
        );
        $s = preg_replace(
            "#&lt;(.*)(\[)(.*)(\])&gt;#isU",
            "&lt;\\1<font color=\"#800080\">\\2\\3\\4</font>&gt;",
            $s
        );

        return nl2br($s);
    }
}
