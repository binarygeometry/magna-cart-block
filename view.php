<?php defined('C5_EXECUTE') or die("Access Denied."); ?>
  <div class="container magna-cart gifz-border-<?= $yhi_border; ?>" id="magna-cart-nation-of-exploit">
    <?php //if($background_color) == true ?>
    <div class="row ">
        <?php if ($yhi_01 === '6x6') : ?> <div class="col-md-6"><?php endif; ?>
        <?php if ($yhi_01 === '8x4') : ?> <div class="col-md-8"><?php endif; ?>
        <?php if ($yhi_01 === '4x8') : ?> <div class="col-md-4"><?php endif; ?>
            <?php if ($yhi_01 === '2x4x4x2') : ?><div class="col-md-4 col-md-offset-2"> <?php endif; ?>
              <?php //$left_uid_01 ;?>
              <div class="rivets">
               <h1 class="seller"><?php echo h($business_name) ;?></h1>
               <p class="pitch"><?php echo h($business_pitch) ;?></p>
              </div>
        </div>

        <?php if ($yhi_01 === '6x6') : ?> <div class="col-md-6"><?php endif; ?>
        <?php if ($yhi_01 === '8x4') : ?> <div class="col-md-4"><?php endif; ?>
        <?php if ($yhi_01 === '4x8') : ?> <div class="col-md-8"><?php endif; ?>
            <?php if ($yhi_01 === '2x4x4x2') : ?> <div class="col-md-4"><?php endif; ?>    
              <?php //$right_uid_01 ;?>
              <div class="rivets">
                <article class="background-picker">
                    <!-- <div class="background-picker-interface"></div> -->
                    <img class="seller_img" src="images/<?php echo h($business_image) ;?>" title="Nation of Exploit Show gif"/>
                </article>
              </div>
        </div>
    </div>
    <div class="row">
        <?php if ($yhi_02 === '6x6') : ?> <div class="col-md-6"><?php endif; ?>
        <?php if ($yhi_02 === '8x4') : ?> <div class="col-md-8"><?php endif; ?>
            <?php if ($yhi_02 === '4x8') : ?> <div class="col-md-4"><?php endif; ?>
        <?php if ($yhi_02 === '2x4x4x2') : ?><div class="col-md-4 col-md-offset-2"> <?php endif; ?>
                <?php //$left_uid_02 ;?>
<!--                 <section class="rivets">
                    <article class="background-picker">
                        <div class="background-picker-interface">Touchscreen RGBS</div>
                        <input type="hidden" value="item_rgb" class="item_rgb">
                        <img src="nation-of-exploit.gif" title="Nation of Exploit Show gif"/>
                        <select name="item_img_src" class="item_image_src">
                            <option value="weird-moth-thing">Weird Moth Thing</option>
                            <option value="woodcutter-ant">Woodcutter Ant</option>
                            <option value="solitary-bee">Solitary Bee</option>
                        </select>
                    </article>
                </section> -->
        </div>

        <?php if ($yhi_02 === '6x6') : ?> <div class="col-md-6"><?php endif; ?>
        <?php if ($yhi_02 === '8x4') : ?> <div class="col-md-4"><?php endif; ?>
        <?php if ($yhi_02 === '4x8') : ?> <div class="col-md-8"><?php endif; ?>
            <?php if ($yhi_02 === '2x4x4x2') : ?> <div class="col-md-4"><?php endif; ?>    
            <?php //$right_uid_02 ;?>
            <section class="rivets">
                <article class="sales-funnel" id="sales-funnel-01">
                        <div class="simpleCart_shelfItem">
                        <h2 class="item_name"><?php echo h($item_name); ?></h2>
                        <input value="1" class="item_Quantity" type="text"><br>
                        <span class="item_price">£<?php echo h($item_price); ?></span><br>
                        <a class="btn item_add" href="javascript:;"> Add to Cart </a></p>
<!--                         <select class="item_size">
                            <option value="Small"> Small </option>
                            <option value="Medium"> Medium </option>
                            <option value="Large"> Large </option>
                        </select><br> -->
                    </div>
                </article>
            </section>

                
        </div>
    </div>
    <div class="row">
        <!-- <?php //if ($yhi_03 === '6x6') : ?> <div class="col-md-6"><?php //endif; ?> -->
        <!-- <?php //if ($yhi_03 === '8x4') : ?> <div class="col-md-8"><?php //endif; ?> -->
        <?php //if ($yhi_03 === '4x8') : ?> 
            <div class="col-md-8 col-md-offset-4">
            <?php //endif; ?>
        <!-- // <?php //if ($yhi_03 === '2x4x4x2') : ?><div class="col-md-4 col-md-offset-2"> <?php //endif; ?> -->
              <div class="rivets">
              <?php // echo $left_uid_03 ;?>

                  <a href="javascript:;" class="simpleCart_checkout"></a>
                    <!-- button to empty the cart -->
                    <a href="javascript:;" class="simpleCart_empty"></a>
                    <!-- show the cart -->
                    <div class="simpleCart_items"></div>
                    <!-- cart total (ex. $23.11)-->
                    <div class="simpleCart_total"></div>
                    <!-- cart quantity (ex. 3) -->
                    <div class="simpleCart_quantity"></div>
                    <!-- tax cost (ex. $1.38) -->
                    <div class="simpleCart_tax"></div>
                    <!-- tax rate (ex. %0.6) -->
                    <div class="simpleCart_taxRate"></div>
                    <!-- shipping (ex. $5.00) -->
                    <div class="simpleCart_shipping"></div>
                    <!-- grand total, including tax and shipping (ex. $28.49) -->
                    <div class="simpleCart_grandTotal"></div>

                        <span class="simpleCart_quantity"></span> items - <span class="simpleCart_total"></span>
                    <a href="javascript:;" class="simpleCart_checkout">Checkout</a>
        </div>
    </div>

        <?php /*//if ($yhi_03 === '6x6') : ?> <div class="col-md-6"><?php endif; ?>
        <?php if ($yhi_03 === '8x4') : ?> <div class="col-md-4"><?php endif; ?>
        <?php if ($yhi_03 === '4x8') : ?> <div class="col-md-8"><?php endif; ?>
        <?php if ($yhi_03 === '2x4x4x2') : ?> <div class="col-md-4"><?php endif; ?>    
              <div class="rivets"><?php $right_uid_03 ;?></div>
        </div>*/?>
    <!-- create a checkout button -->

    <?php /*
    <div class="row">
        <?php if ($yhi_04 === '6x6') : ?> <div class="col-md-6"><?php endif; ?>
        <?php if ($yhi_04 === '8x4') : ?> <div class="col-md-8"><?php endif; ?>
        <?php if ($yhi_04 === '4x8') : ?> <div class="col-md-4"><?php endif; ?>
        <?php if ($yhi_04 === '2x4x4x2') : ?><div class="col-md-4 col-md-offset-2"> <?php endif; ?>
              <div class="rivets"><?= $left_uid_04 ;?></div>
        </div>

        <?php if ($yhi_04 === '6x6') : ?> <div class="col-md-6"><?php endif; ?>
        <?php if ($yhi_04 === '8x4') : ?> <div class="col-md-4"><?php endif; ?>
        <?php if ($yhi_04 === '4x8') : ?> <div class="col-md-8"><?php endif; ?>
        <?php if ($yhi_04 === '2x4x4x2') : ?> <div class="col-md-4"><?php endif; ?>    
              <div class="rivets"><?= $right_uid_04 ;?></div>
        </div>
    </div>
*/?>

</div>
    <!-- // <script src="simpleCart.js"></script> -->
    <script>
    $(function() {
    console.log( "ready!" );
      simpleCart({
        checkout: {
          type: "PayPal",
          email: "<?php echo h($business_email); ?>"
        }
      });
});
    </script>
